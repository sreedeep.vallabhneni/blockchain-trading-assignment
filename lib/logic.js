/**
 * Process buy request from a user
 * @param {org.ggktech.trading.BuyAssetRequest} buyAssetRequest - the trade to be processed
 * @transaction
 */
async function assetBuyRequest(buyAssetRequest) {
    if(buyAssetRequest.asset1.status == "NOT_FOR_SALE"){
        throw new Error('Asset not for sale!')
    } else if(buyAssetRequest.toUser.balance < buyAssetRequest.asset1.price){
        throw new Error('Not enough balance!')
    } else {
        buyAssetRequest.asset1.owner.balance += buyAssetRequest.asset1.price;
        buyAssetRequest.toUser.balance -= buyAssetRequest.asset1.price;
        let oldOwner = buyAssetRequest.asset1.owner;
        buyAssetRequest.asset1.owner = buyAssetRequest.toUser;
        let asset1Registry = await getAssetRegistry('org.ggktech.trading.Asset1');
        await asset1Registry.update(buyAssetRequest.asset1);
        let userRegistry = await getParticipantRegistry('org.ggktech.trading.User');
        await userRegistry.update(buyAssetRequest.asset1.owner);
        await userRegistry.update(oldOwner);
    }
}

/**
 * Change status of an asset
 * @param {org.ggktech.trading.StatusChangeRequest} statusChangeRequest - the trade to be processed
 * @transaction
 */
async function changeStatusRequest(statusChangeRequest) {
    if(statusChangeRequest.asset1.status == "FOR_SALE"){
        statusChangeRequest.asset1.status = "NOT_FOR_SALE";
    } else {
        statusChangeRequest.asset1.status = "FOR_SALE";
    }
    let asset1Registry = await getAssetRegistry('org.ggktech.trading.Asset1');
    await asset1Registry.update(statusChangeRequest.asset1);
}

